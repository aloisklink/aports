# Contributor: Anjandev Momi <anjan@momi.ca>
# Maintainer: Anjandev Momi <anjan@momi.ca>
pkgname=jami-daemon
pkgver=4.0.0_git20230323
pkgrel=0
_gitrev=c5bbd21e1c125f4012e2f88f485816a438ae62a7
_pjprojectver=e4b83585a0bdf1523e808a4fc1946ec82ac733d0
pkgdesc="Free and universal communication platform which preserves the users’ privacy and freedoms (daemon component)"
url="https://jami.net"
arch="all !ppc64le !s390x !riscv64" # no webrtc-audio-processing
license="GPL-3.0-or-later"
makedepends="
	alsa-lib-dev
	asio-dev
	eudev-dev
	fmt-dev
	ffmpeg-dev
	gnutls-dev
	jsoncpp-dev
	libarchive-dev
	libgit2-dev
	libsecp256k1-dev
	libupnp-dev
	meson
	msgpack-cxx-dev
	opendht-dev
	pulseaudio-dev
	restinio-dev
	speex-dev
	speexdsp-dev
	sqlite-dev
	webrtc-audio-processing-dev
	yaml-cpp-dev
	zlib-dev
	"
subpackages="$pkgname-dev"
source="$pkgname-$_gitrev.tar.gz::https://git.jami.net/savoirfairelinux/jami-daemon/-/archive/$_gitrev/jami-daemon-$_gitrev.tar.gz
	pjproject-$_pjprojectver.tar.gz::https://github.com/savoirfairelinux/pjproject/archive/$_pjprojectver/pjproject-$_pjprojectver.tar.gz
	gcc13.patch
	opendht-2.5.patch
	"
builddir="$srcdir/$pkgname-$_gitrev"
options="!check" # https://git.jami.net/savoirfairelinux/jami-daemon/-/issues/649

prepare() {
	default_prepare
	cp ../pjproject-$_pjprojectver.tar.gz contrib/tarballs
	mkdir contrib/native
}

build() {
	cd contrib/native
	../bootstrap \
		--disable-downloads \
		--disable-all \
		--enable-pjproject
	make DEPS_pjproject=

	cd "$builddir"
	abuild-meson \
		-Dinterfaces=library \
		-Dopensl=disabled \
		-Dportaudio=disabled \
		-Dtests=false \
		-Dpkg_config_path="$builddir/contrib/$(cc -dumpmachine)/lib/pkgconfig" \
		. output
	meson compile -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

sha512sums="
4579e2d95509db05d2a3285726c1ab0ba16c124392d1024edab5c8b32787af5019941263e2286547410b83ced0a040ad6280e297bb0c451a8838025ede2f7108  jami-daemon-c5bbd21e1c125f4012e2f88f485816a438ae62a7.tar.gz
617f3a5cd38423a429b17553bb73ad467694ce3e899a788aaf20d0540f6a6904c06a2fc35bf9e0d1c9ab07ba821ca0e1d6d2b002e473bfa9850ff6f8b96447c0  pjproject-e4b83585a0bdf1523e808a4fc1946ec82ac733d0.tar.gz
aab0320256da3971a28e7190a7992612ce7a2da8402a2ffdc054dcc13ff56739ccfaad8eaa5e2d37e22b9a45b4b638d87ea22002bce519380e3c83e317883a9f  gcc13.patch
755d260f9f8deda985dc448898db1ab4250dbd2c7261278a8017ffbe996fd62a1b3298d4450c6a5588fefa022ac3d97ae69e55f53bd57667c5aa414bd971444e  opendht-2.5.patch
"
