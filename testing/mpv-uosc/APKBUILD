# Maintainer: psykose <alice@ayaya.dev>
pkgname=mpv-uosc
pkgver=4.7.0
pkgrel=1
pkgdesc="Feature-rich minimalist proximity-based UI for mpv"
url="https://github.com/tomasklaen/uosc"
arch="noarch"
# https://github.com/tomasklaen/uosc/issues/586
license="GPL-3.0"
depends="mpv"
makedepends="unzip"
source="$pkgname-$pkgver.zip.noauto::https://github.com/tomasklaen/uosc/releases/download/$pkgver/uosc.zip"
builddir="$srcdir"
options="!check" # no tests

package() {
	mkdir -p "$pkgdir"/etc/mpv "$pkgdir"/etc/apk/protected_paths.d/
	unzip "$srcdir"/$pkgname-$pkgver.zip.noauto -d "$pkgdir"/etc/mpv/

	# mpv only reads user dirs and /etc/mpv, but we want to always override these files,
	# because they are actually the plugin and not configuration, and not meant to be edited.
	# so, exclude the dirs so they are not upgrade protected
	cat > "$pkgdir"/etc/apk/protected_paths.d/mpv-uosc.list <<-EOF
	-etc/mpv/fonts
	-etc/mpv/scripts
	EOF
}

sha512sums="
3d729e8165e9b53ce061514b7ee75d68e264dfa96ab7ef6c5e78cd0a28db9c1eabcfe99e51f46d2f77f30c79cdd81988798efe1182c8fa83ad9bd1ce697504e7  mpv-uosc-4.7.0.zip.noauto
"
