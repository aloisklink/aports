# Contributor: Sam Nystrom <sam@samnystrom.dev>
# Maintainer: Sam Nystrom <sam@samnystrom.dev>
pkgname=lutgen
pkgver=0.8.0
pkgrel=0
pkgdesc="Blazingly fast interpolated LUT generator and applicator for arbitrary and popular color palettes"
url="https://github.com/ozwaldorf/lutgen-rs"
arch="all"
license="MIT"
makedepends="cargo cargo-auditable"
subpackages="
	$pkgname-bash-completion
	$pkgname-zsh-completion
	$pkgname-fish-completion
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/ozwaldorf/lutgen-rs/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/lutgen-rs-$pkgver"
options="net" # cargo fetch

prepare() {
	default_prepare
	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --release --frozen
	mkdir -p completions
	./target/release/lutgen completions bash > completions/lutgen
	./target/release/lutgen completions zsh > completions/_lutgen
	./target/release/lutgen completions fish > completions/lutgen.fish
}

check() {
	cargo test --frozen
}

package() {
	install -Dm755 target/release/lutgen "$pkgdir"/usr/bin/lutgen
	install -Dm644 completions/lutgen "$pkgdir"/usr/share/bash-completion/completions/lutgen
	install -Dm644 completions/_lutgen "$pkgdir"/usr/share/zsh/site-functions/_lutgen
	install -Dm644 completions/lutgen.fish "$pkgdir"/usr/share/fish/vendor_completions.d/lutgen.fish
}

sha512sums="
ebec81f4959f034cc8c2f14dafd0fb16fe429f16a0353926f98702cc47e09108c0253f3a00aa14b3cedfbe799090e4f16aa3048d5b143816f572bcec288f4591  lutgen-0.8.0.tar.gz
"
