# Maintainer: Hugo Osvaldo Barrera <hugo@whynothugo.nl>
pkgname=xendmail
pkgver=0.2.0
pkgrel=0
pkgdesc="Like sendmail, for users"
url="https://git.sr.ht/~whynothugo/xendmail"
# rust-ring
arch="all !s390x !ppc64le !riscv64"
license="Apache-2.0"
makedepends="cargo cargo-auditable"
source="xendmail-$pkgver.tar.gz::https://git.sr.ht/~whynothugo/xendmail/archive/v$pkgver.tar.gz"
builddir="$srcdir/$pkgname-v$pkgver"
options="net" # fetch dependencies

prepare() {
	default_prepare

	cargo fetch --locked
}

build() {
	cargo auditable build --release --frozen
}

check() {
	cargo test --frozen
}

package() {
	install -Dm 0755 target/release/$pkgname "$pkgdir/usr/bin/$pkgname"
	install -dm 0755 "$pkgdir/usr/sbin"
	ln -s /usr/bin/$pkgname "$pkgdir/usr/sbin/sendmail"
}

sha512sums="
2c75386e57a79c9e2071669232e9e4130524987277bfa65585339fb472b562700471787ff30ee10c3826263e1b417634b693f08b3f5dfdfbd5d7bb09a8aa00f3  xendmail-0.2.0.tar.gz
"
