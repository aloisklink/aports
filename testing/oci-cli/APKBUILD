# Contributor: Adam Bruce <adam@adambruce.net>
# Maintainer: Adam Bruce <adam@adambruce.net>
pkgname=oci-cli
pkgver=3.29.4
pkgrel=0
pkgdesc="Oracle Cloud Infrastructure CLI"
url="https://docs.oracle.com/en-us/iaas/Content/API/Concepts/cliconcepts.htm"
arch="noarch"
license="UPL-1.0 OR Apache-2.0"
depends="
	python3
	py3-arrow
	py3-certifi
	py3-click
	py3-cryptography
	py3-dateutil
	py3-jmespath
	py3-oci
	py3-openssl
	py3-prompt_toolkit
	py3-six
	py3-terminaltables
	py3-tz
	py3-yaml
	"
makedepends="
	py3-gpep517
	py3-setuptools
	py3-wheel
	"
subpackages="$pkgname-pyc"
source="$pkgname-$pkgver.tar.gz::https://github.com/oracle/oci-cli/archive/refs/tags/v$pkgver.tar.gz"
options="!check" # Cannot test as OCI resource identifiers are required as environment variables

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl

	find "$pkgdir"/usr/lib/python* -type d -name "tests" -exec rm -r {} \+
}

sha512sums="
ba91224708bfaadd0b78aa279f0a8d73426a98ceab5241f7f8e0bca7847b2f9d103b25949c9d420c5ab4532998a88e8f9724b94c3a4a456cab872d8c729d52cb  oci-cli-3.29.4.tar.gz
"
