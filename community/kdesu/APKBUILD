# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=kdesu
pkgver=5.108.0
pkgrel=1
pkgdesc="Integration with su for elevated privileges"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-only"
depends_dev="
	kcoreaddons-dev
	ki18n-dev
	kpty-dev
	kservice-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	qt5-qttools-dev
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kdesu-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
# Since the goal of this library is to elevate privileges, suid being needed should be obvious
options="suid"

[ "$CARCH" = "ppc64le" ] && options="$options !check" # kdesutest is broken

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/frameworks/kdesu.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	xvfb-run ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
329a34b281f67cf0b0ac69ed833cb30b7b3405458be5fccc892b1b023a7f80e0976d2b68cbdb6a9a7cb428414d575b9a1f48c8388eab226607c6130a32556aae  kdesu-5.108.0.tar.xz
"
