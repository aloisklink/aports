# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=qqc2-breeze-style
pkgver=5.27.6
pkgrel=0
pkgdesc="Breeze inspired QQC2 style"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://www.plasma-mobile.org/"
license="LicenseRef-KDE-Accepted-LGPL AND LicenseRef-KFQF-Accepted-GPL"
depends="kirigami2"
makedepends="
	extra-cmake-modules
	kconfig-dev
	kconfigwidgets-dev
	kguiaddons-dev
	kiconthemes-dev
	kirigami2-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtquickcontrols2-dev
	qt5-qtx11extras-dev
	samurai
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/qqc2-breeze-style-$pkgver.tar.xz"
subpackages="$pkgname-dev"
options="!check" # No tests

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/plasma/qqc2-breeze-style.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
64cf5201d3e0729a3a66d781ad8567486459efb07e35bcf919d0bdfe9aa6e42dd26ca6fbfd783487daa0e0fa9359ce25aedb8bc9aef8a46e378da41c6c05881f  qqc2-breeze-style-5.27.6.tar.xz
"
