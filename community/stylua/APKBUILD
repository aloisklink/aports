# Contributor: psykose <alice@ayaya.dev>
# Maintainer: psykose <alice@ayaya.dev>
pkgname=stylua
pkgver=0.18.1
pkgrel=0
pkgdesc="Opinionated Lua code formatter"
url="https://github.com/JohnnyMorganz/StyLua"
arch="all !s390x !riscv64" # blocked by cargo
license="MPL-2.0"
makedepends="cargo cargo-auditable"
source="$pkgname-$pkgver.tar.gz::https://github.com/JohnnyMorganz/StyLua/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/StyLua-$pkgver"

_features="--no-default-features --features lua54,strum,serialize"

prepare() {
	default_prepare
	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --release --frozen $_features
}

check() {
	cargo test --frozen $_features
}

package() {
	install -Dm755 target/release/stylua -t "$pkgdir"/usr/bin/
}

sha512sums="
4586305d1276a7ebe40306fe8ab97157d1a819814f224bd7ea8386cb5dc9effeb8ccd4f52fb428329336db3b41b42ba36ebf34552d0a6180d48d86d29b04fd4d  stylua-0.18.1.tar.gz
"
