# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=kdav
pkgver=5.108.0
pkgrel=1
pkgdesc="A DAV protocol implementation with KJobs"
url="https://community.kde.org/Frameworks"
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
license="LGPL-2.0-or-later"
depends_dev="
	kcoreaddons-dev
	ki18n-dev
	kio-dev
	qt5-qtbase-dev
	qt5-qtxmlpatterns-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	qt5-qttools-dev
	samurai
	"
checkdepends="xvfb-run"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kdav-$pkgver.tar.xz"

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/frameworks/kdav.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	xvfb-run ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
cbdd1beca14f3b43aca5645a859527c7bde1b677605edb4092d4f4cd36e6f8300f1fc02fb1619ffa5e83281642afbeffbe5ea4d3d9ac1bc7c25d18fd325ecb71  kdav-5.108.0.tar.xz
"
