# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kio-extras
pkgver=23.04.3
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://www.kde.org/applications/internet/"
pkgdesc="Additional components to increase the functionality of KIO"
license="GPL-2.0-or-later (LGPL-2.1-only OR LGPL-3.0-only)"
depends="kinit"
makedepends="
	extra-cmake-modules
	gperf
	kactivities-dev
	karchive-dev
	kbookmarks-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kdbusaddons-dev
	kdnssd-dev
	kdoctools-dev
	kdsoap-dev
	kguiaddons-dev
	khtml-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	kpty-dev
	libmtp-dev
	libssh-dev
	nfs-utils-dev
	phonon-dev
	qt5-qtbase-dev
	qt5-qtsvg-dev
	samba-dev
	samurai
	shared-mime-info
	solid-dev
	syntax-highlighting-dev
	"
checkdepends="xvfb-run dbus"
source="https://download.kde.org/stable/release-service/$pkgver/src/kio-extras-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

_commit=""
_repo_url="https://invent.kde.org/network/kio-extras.git"
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 $_repo_url .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	dbus-run-session -- xvfb-run ctest --test-dir build --output-on-failure \
		-E "testkioarchive|thumbnailtest"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
f44a74a43b7b3cc54811405e6e671cc50754d054ef8e0456f2ab54f57178b4852037f5e7e56cc5c07aa391c5e6012dd07ed134ace68a80e2b880b248f3757ca3  kio-extras-23.04.3.tar.xz
"
