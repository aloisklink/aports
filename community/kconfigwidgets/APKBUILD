# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=kconfigwidgets
pkgver=5.108.0
pkgrel=1
pkgdesc="Widgets for KConfig"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-only AND LGPL-2.1-or-later"
depends_dev="
	kauth-dev
	kcodecs-dev
	kconfig-dev
	kcoreaddons-dev
	kguiaddons-dev
	ki18n-dev
	kwidgetsaddons-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	graphviz
	kdoctools-dev
	qt5-qttools-dev
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kconfigwidgets-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/frameworks/kconfigwidgets.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	xvfb-run ctest --test-dir build --output-on-failure -E "kstandardactiontest"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
6f01439c93bd52b3d398e6e2cf79da10a5370eb4710bc3153f879bc8da1b6b8fa704f9c47e56d4463feed936d49085f4c70a47a27b699d3252167b5b78f19eab  kconfigwidgets-5.108.0.tar.xz
"
