# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=plasma-desktop
pkgver=5.27.6
pkgrel=0
pkgdesc="KDE Plasma Desktop"
# armhf blocked by qt5-qtdeclarative
# ppc64le, s390x and riscv64 blocked by qt5-qtwebengine -> kaccounts-integration
arch="all !armhf !ppc64le !s390x !riscv64"
url='https://kde.org/plasma-desktop/'
license="GPL-2.0-only AND LGPL-2.1-only"
depends="
	accountsservice
	font-noto-emoji
	ibus-emoji
	kirigami2
	plasma-workspace
	qqc2-desktop-style
	setxkbmap
	xdg-user-dirs
	"
makedepends="
	attica-dev
	baloo-dev
	eudev-dev
	extra-cmake-modules
	fontconfig-dev
	ibus-dev
	kaccounts-integration-dev
	kactivities-dev
	kactivities-stats-dev
	kauth-dev
	kcmutils-dev
	kcodecs-dev
	kconfig-dev
	kdbusaddons-dev
	kdeclarative-dev
	kdelibs4support-dev
	kdoctools-dev
	kemoticons-dev
	kglobalaccel-dev
	ki18n-dev
	kitemmodels-dev
	knewstuff-dev
	knotifications-dev
	knotifyconfig-dev
	kpeople-dev
	kpipewire-dev
	krunner-dev
	kwallet-dev
	kwin-dev
	libxcursor-dev
	libxi-dev
	libxkbfile-dev
	plasma-framework-dev
	plasma-workspace-dev
	qqc2-desktop-style-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtsvg-dev
	qt5-qtx11extras-dev
	samurai
	signon-plugin-oauth2-dev
	xf86-input-evdev-dev
	xf86-input-libinput-dev
	xf86-input-synaptics-dev
	xkeyboard-config-dev
	"
checkdepends="
	dbus
	iso-codes
	xvfb-run
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/plasma-desktop-$pkgver.tar.xz"
subpackages="$pkgname-dbg $pkgname-doc $pkgname-lang knetattach"

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/plasma/plasma-desktop.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	dbus-run-session -- xvfb-run ctest --test-dir build --output-on-failure -E "positionertest"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

knetattach() {
	pkgdesc="Wizard which makes it easier to integrate network resources with the Plasma Desktop"
	depends="kdelibs4support"

	amove usr/bin/knetattach
	amove usr/share/applications/org.kde.knetattach.desktop
}

sha512sums="
b1524c54b1ff5d45ae56883fef2fe34927991eba4b28e697867a1097baa8d6a235fc38e11385b1f02c78c5700e669ad902a721c089bb571accb13fd734c1fdb1  plasma-desktop-5.27.6.tar.xz
"
