# Contributor: Fabian Affolter <fabian@affolter-engineering.ch>
# Maintainer: Fabian Affolter <fabian@affolter-engineering.ch>
pkgname=py3-zeroconf
pkgver=0.71.4
pkgrel=0
pkgdesc="Python implementation of multicast DNS service discovery"
url="https://github.com/jstasiak/python-zeroconf"
arch="all"
license="LGPL-2.0-or-later"
replaces="py-zeroconf" # for backwards compatibility
provides="py-zeroconf=$pkgver-r$pkgrel" # for backwards compatibility
depends="python3 py3-ifaddr"
makedepends="
	cython
	py3-gpep517
	py3-poetry-core
	py3-setuptools
	py3-wheel
	python3-dev
	"
checkdepends="py3-pytest py3-pytest-cov py3-pytest-asyncio"
subpackages="$pkgname-pyc"
source="python-zeroconf-$pkgver.tar.gz::https://github.com/jstasiak/python-zeroconf/archive/refs/tags/$pkgver.tar.gz"
builddir="$srcdir"/python-zeroconf-$pkgver

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
322b65fd72c2176f1738685df3cae0287f583808141e4bf7a2c432bb6ad9a779a00f3d9a7bb0c5570b86cce03f94e461d4f2e3c2908e2f631132b81e095361e0  python-zeroconf-0.71.4.tar.gz
"
